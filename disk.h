//
// Created by Jimmy Ramos on 21-Aug-16.
//

#ifndef FILESYSTEM_DISCO_H
#define FILESYSTEM_DISCO_H
#include <cstdint>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include "super_block.h"
#include "entry.h"

#define BLOCK_SIZE  4096      /* TAMANIO DE BLOQUE EN  "DISCO" en bytes*/
#define NUM_FILES 128        /* Maximo cantidad de entradas archivos que caben en un bloque de 4kb */


class disk {
public:
    static int make_disk(char *name, uint64_t *disk_blocks, uint64_t * disk_size);

    static int close_disk(char *disk_name);    /* close a previously opened disk (file)       */

    static void show_information(char *disk_name);

    static super_block get_super_block(char *disk_name);

    static void make_ls(char * disk_name);

    static void rename_file(char* disk_name,char *old_name, char *new_name);

    static int get_entry_by_name(char* disk_name,char *name);

    static int delete_file(char* disk_name,char *name);

    static int get_deleted_entry_position(char* disk_name);

    static int make_file(char *disk_name, char *name,unsigned int first_block, int size);

};

#endif //FILESYSTEM_DISCO_H
