# README #

Proyecto para la clase de Sistemas Operativos 2 - Unitec - Agosto 2016

### ¿De qué es el proyecto? ###

* Un File System sencillo donde se pueden crear discos, montarlos y desmontarlos, crear archivos y hacer operaciones básicas con ellos (escribir, listar, copiar, borrar, renombrar) 


### ¿Cuáles son las estructuras que sigo? ###

* La estructura del File System tiene varios tipos de bloques escritos en este orden:

Superbloque | Bloque de entradas | Bloques de mapa de bits | Bloques de datos

* Superbloque : Contiene información como ser:
  -Tamaño del disco (Este se envía de parámetro cuando creamos una partición o disco, ejemplo: createdisk 1024 creará un disco de 1024 MB
  
  -Bit de montado : indicador si el disco está montado (esto permite restringir las operaciones solo a los discos que estén montados)
   
  -Cantidad de Bloques: Este se calcula al recibir el tamaño del disco, partiendo de que cada bloque es de 4KB cada uno.

  -Cantidad de bloques de Bitmap: Cuantos bloques que ocupa el mapa de bits necesario para direccionar la cantidad de bloques de la partición.

  -Cantidad de entradas : Un contador de los archivos que se encuentran escritos en el disco.

*  Bloque de entradas: Este bloque es el que almacenará la metadata de todos los archivos que puedo escribir en mi sistema de archivos, este bloque puede alojar solamente 128 entradas o archivos dado que solo alocamos un bloque para este fin y la estructura entry tiene un tamaño de 32 bytes

  Entry tiene: Nombre del archivo, fecha de creación, tamaño y apuntador al primer bloque de datos

* Bloques de mapa de bits: almacenar el mapa de bits requiere guardar esa información en disco por lo que se destinan x cantidad de bloques para ello, esta cantidad de bits depende de la cantidad de bloques que tendrá la partición.

* Bloques de datos : esto es lo que tienen disponible todas las entradas para crecer en datos y donde estará la información, su control se lleva en el mapa de bits.

### ¿Cómo manejo la lectura y escritura?###

* Para manejar escritura y lectura se usan bloques de 4kb para todos los bloques, incluidos los de metadata (el superbloque, la tabla de entradas). En lo que respecta a el superbloque, este se reescribe cuando hay nuevas entradas (actualizar el int de cantidad de entradas) y cuando se monta y desmonta la partición.

* La escritura de las entradas de realiza de otra manera ya que no se reescribe todo el bloque sino que solo se escribe la nueva entrada de 32 bytes, se hace un Append al final de ese bloque de entradas