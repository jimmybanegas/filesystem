//
// Created by Affisa-Jimmy on 1/9/2016.
//

#ifndef FILESYSTEM_DATA_BLOCK_H
#define FILESYSTEM_DATA_BLOCK_H


class data_block {
public:
    char data [BLOCK_SIZE - sizeof(int)];
    int next_block;
};

#endif //FILESYSTEM_DATA_BLOCK_H