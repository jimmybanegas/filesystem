//
// Created by Jimmy Ramos on 25-Aug-16.
//

#ifndef FILESYSTEM_BITMAP_H
#define FILESYSTEM_BITMAP_H

#define BIT (8*sizeof(byte))
#define BITMAP_NOTFOUND -1

enum: bool {FALSE = 0, TRUE =1};
typedef unsigned char byte;

bool bitmapGet   (byte *, int);
void bitmapSet   (byte *, int);
void bitmapReset (byte *, int);
int  bitmapSearch(byte *, bool, int, int);


#endif