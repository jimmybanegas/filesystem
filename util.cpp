//
// Created by Jimmy Ramos on 21-Aug-16.
//

#include "util.h"
#include "disk.h"
#include "entry.h"
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <time.h>

FILE *disk;

int util::findCmd(char* command)
{
    if(strcmp("menu", command) == 0)
        return 0;
    if(strcmp("ls", command) == 0)
        return 1;
    if(strcmp("create_block", command) == 0)
        return 2;
    if(strcmp("empty", command) == 0)
        return 3;
    if(strcmp("delete", command) == 0)
        return 4;
    if(strcmp("rename", command) == 0)
        return 5;
    if(strcmp("copy_from_fs", command) == 0)
        return 6;
    if(strcmp("copy_to_fs", command) == 0)
        return 7;
    if(strcmp("mount", command) == 0)
        return 8;
    if(strcmp("unmount", command) == 0)
        return 9;
    if(strcmp("info", command) == 0)
        return 10;
    if(strcmp("quit", command) == 0)
        return 11;
    if(strcmp("delete_block", command) == 0)
        return 12;
    if(strcmp("clear", command) == 0)
        return 13;
    else
        return -1;
}

int util::read_block(char * disk_name,char *buf, int block_number) {

    disk = fopen(disk_name, "rb");
    if (disk == NULL){
        printf("Disco no encontrado\n");
        return -1;
    }
    else{
        fseek(disk, block_number * BLOCK_SIZE, SEEK_SET); // Byte donde inicia la FAT
        fread(buf, 1, BLOCK_SIZE, disk);
    }
    fclose(disk);
    return 0;
}

int util::write_block(char *disk_name,char *buf, int block_number) {

    disk = fopen(disk_name, "r+b"); // Sustituir el contenido
    /*if (f == NULL)
        printf("Error al escribir\n" );
    else{*/
    fseek(disk, block_number * BLOCK_SIZE, SEEK_SET); // Byte donde inicia la FAT
    fwrite(buf, 1, BLOCK_SIZE, disk); // opcion 2: buffer, 1, sizeof(char) * nDisco.header.BlockSize, f
    //}
    fclose(disk);
    return 0;
}

int util::write_block_copyin(char *disk_name,char *buf, int block_number,int prox_bloque) {

    int tam_bufer (BLOCK_SIZE- sizeof(int));

    char* buffer = (char*) malloc (sizeof(int));
    memcpy(buffer, &prox_bloque, BLOCK_SIZE);

    disk = fopen(disk_name, "r+b"); // Sustituir el contenido
    /*if (f == NULL)
        printf("Error al escribir\n" );
    else{*/
    fseek(disk, block_number * BLOCK_SIZE, SEEK_SET); // Byte donde inicia la FAT
    fwrite(buf, 1, tam_bufer, disk); // opcion 2: buffer, 1, sizeof(char) * nDisco.header.BlockSize, f

    fseek(disk, (block_number * BLOCK_SIZE)+tam_bufer, SEEK_SET); // Byte donde inicia la FAT
    fwrite(buffer, 1, sizeof(int), disk); // opcion 2: buffer, 1, sizeof(char) * nDisco.header.BlockSize, f

    //}
    fclose(disk);
    return 0;
}

int util::write_entry_in_block(char *disk_name, char *buf, int block_number, int entries_quantity) {

    disk = fopen(disk_name, "r+b"); // Sustituir el contenido

    fseek(disk, (block_number * BLOCK_SIZE) + (entries_quantity* sizeof(entry)), SEEK_SET); // Byte donde inicia la FAT
    fwrite(buf, 1, sizeof(entry), disk); // opcion 2: buffer, 1, sizeof(char) * nDisco.header.BlockSize, f
    //}

    fclose(disk);
    return 0;
}

int util::read_bitmap_in_block(char *disk_name, char *buf, int block_number, int blocks_quantity) {
    disk = fopen(disk_name, "rb");
    if (disk == NULL)
        printf("Disco no encontrado\n");
    else{
        fseek(disk, (block_number * BLOCK_SIZE) , SEEK_SET); // Byte donde inicia la FAT
        fread(buf, 1, (blocks_quantity)*BLOCK_SIZE, disk);
    }
    fclose(disk);
    return 0;
}

int util::write_bitmap_in_block(char *disk_name, char *buf, int block_number, int blocks_quantity) {
    disk = fopen(disk_name, "r+b"); // Sustituir el contenido

    fseek(disk, (block_number * BLOCK_SIZE) , SEEK_SET); // Byte donde inicia la FAT
    fwrite(buf, 1, (blocks_quantity)*BLOCK_SIZE, disk); // opcion 2: buffer, 1, sizeof(char) * nDisco.header.BlockSize, f
    //}

    fclose(disk);
    return 0;
}

int util::read_entry_in_block(char *disk_name, char *buf, int block_number, int entry_position) {
    disk = fopen(disk_name, "rb");
    if (disk == NULL)
        printf("Disco no encontrado\n");
    else{
        fseek(disk, (block_number * BLOCK_SIZE) + (entry_position* sizeof(entry)) , SEEK_SET); // Byte donde inicia la FAT
        fread(buf, 1, sizeof(entry), disk);
    }
    fclose(disk);
    return 0;
}

void util::findCmdMessage(char *command) {
    if(strcmp("create_block", command) == 0)
        printf("create_block <device_name> <size> <GB or MB>\r\n");
    if(strcmp("empty", command) == 0)
        printf("empty <filename>\r\n");
    if(strcmp("delete", command) == 0)
        printf("delete <filename>\r\n");
    if(strcmp("rename", command) == 0)
        printf("rename <old_name> <new_name>\r\n");
    if(strcmp("copy_from_fs", command) == 0)
        printf("copy_from_fs <source_path_fs> <target_filename_myfs>\r\n");
    if(strcmp("copy_to_fs", command) == 0)
        printf("copy_to_fs <source_filename_myfs> <target_path_fs>\r\n");
    if(strcmp("mount", command) == 0)
        printf("mount <device_name>\r\n");
    if(strcmp("delete_block", command) == 0)
        printf("delete_block <device_name>\r\n");
}




