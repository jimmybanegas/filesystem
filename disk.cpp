//
// Created by Jimmy Ramos on 21-Aug-16.
//

#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <sys/fcntl.h>
#include "disk.h"
#include "util.h"
#include "data_block.h"
#include "bitmap.h"
#include <bitset>
#include <array>
#include <ctime>
#include <iomanip>
#include <cmath>

super_block superBlock;


    int disk::make_disk(char *name, uint64_t  *disk_blocks, uint64_t *disk_size) {

        //Calcular el tamanio del mapa de bits
        uint64_t x = *disk_blocks;
        uint64_t bitmap_blocks_quantity;

        printf("Disk blocks: %lu\n", x);
        printf("\n");

        //Dividir la cantidad de bloques entre ocho porque cada bloque se representa con un bit y queremos bytes para sacar cant de bloques
        bitmap_blocks_quantity = (x / 8) / BLOCK_SIZE;

        //Si el mapa ocupa menos de 4096 bytes hay que asignar un bloque por lo menos
        if(bitmap_blocks_quantity <= 0)
            bitmap_blocks_quantity = 1;

        FILE * disk;
        //Crear el disco con ek nombre que viene de parametro
        disk = fopen( name, "wb");
        fclose(disk);

        if(disk == NULL){
            printf("Couldn't create disk: %lu\n", x);
            printf("\n");
            return -1;
        }

        //Escribir el superbloque
        superBlock.size = *disk_size;
//        strcpy((char *) &superBlock.name, name);
        superBlock.mounted = 0; //Al inicializarlo no estara montado este disco
        superBlock.block_count = * disk_blocks;
        superBlock.bitmap_blocks = bitmap_blocks_quantity;
        superBlock.entries_blocks = 1; //Solo habra un bloque de 4kb con 128 entradas, cada struct entry mide 32 bytes
        superBlock.entries_quantity = 0; //Al inicializarlo no habra ninguna entrada

        char* buffer = (char*) malloc (BLOCK_SIZE);
        memcpy(buffer, &superBlock, sizeof(superBlock));

        util::write_block(name,buffer, 0);

        delete[] buffer;

        //Escribir el mapa de bits
        //Cantidad de bits necesarios para direccionar la cantidad de bloque
        //Tomar en cuenta que cada byte (1 char) puede usarse para direccionar 8 bloques porque 1 byte = 8 bits
        int bits_needed = (int) (superBlock.block_count / BIT);

        printf("Cantidad de bits necesarios :%d\n",bits_needed);
        printf("\n");

        //Inicializacion de el arreglo de chars en cero para indicar que todos estan libres
        char  mapa_bits[bits_needed];
        int i = 0; char init = 0;

        while (i < bits_needed)
            mapa_bits[i++] = init;

        mapa_bits[i]='\0';

        //Marcar los bloques de superbloque (ocupa 1 bloque), tabla de entradas (ocupa 1 bloque)
        // y mapa de bits (ocupa superBlock.bitmap_blocks) como ocupados en el mapa de bits antes de escribirlo

        for (int j = 0; j < (2+ superBlock.bitmap_blocks) ; ++j) {
            bitmapSet((byte *) mapa_bits, j+1);
        }

        printf("Size of mapabits: %d\n", sizeof(mapa_bits));
        printf("\n");

        char* buffer2 = (char*) malloc (BLOCK_SIZE * superBlock.bitmap_blocks);
        memcpy(buffer2, &mapa_bits, sizeof(mapa_bits));

        util::write_bitmap_in_block(name,buffer2, 2, (int) superBlock.bitmap_blocks);

        delete[] buffer2;

        //Terminar el formateo del disco con todas sus estructuras necesarias

        return 0;
    }

    int disk::close_disk(char * disk_name) {
        super_block leer;

        char* buffer2 = new char[BLOCK_SIZE];// malloc(sizeof(SuperBlock));
        util::read_block(disk_name, buffer2, 0);
        memcpy(&leer, buffer2, sizeof(leer));

        delete[] buffer2;

        leer.mounted = 0;
        leer.block_count = leer.block_count;
        leer.entries_blocks = leer.entries_blocks;
        leer.bitmap_blocks = leer.bitmap_blocks;
        leer.size = leer.size;
        leer.entries_quantity = leer.entries_quantity;

        char* buffer = (char*) malloc (BLOCK_SIZE);
        memcpy(buffer, &leer, sizeof(leer));

        util::write_block(disk_name, buffer, 0);

        delete[] buffer;

        printf("Disk unmounted\n");
        printf("\n");

        return 0;

    }

    void disk::show_information(char *disk_name) {
        super_block leer;
        if(disk_name != NULL && leer.mounted){

            char* buffer2 = new char[BLOCK_SIZE];
            util::read_block(disk_name,buffer2,0);
            memcpy(&leer, buffer2, sizeof(leer));

            delete[] buffer2;
            printf("\n");
            printf("Size of superbloque: %d\n", sizeof(leer));
            printf("Tamanio Disco : %lu\n",leer.size);
            printf("Cantidad Bloques : %lu\n",leer.block_count);
            printf("Tamanio bloques entrada : %lu\n",leer.entries_blocks);
            printf("cantidad Bloques bitmap : %lu\n",leer.bitmap_blocks);
            printf("cantidad entradas : %lu\n",leer.entries_quantity);
            printf("Montado %d\n", (int) leer.mounted);
        }
        else
            printf("No mounted disk, please mount one \n");
            printf("\n");
    }

    int disk::make_file(char *disk_name, char *name,unsigned int first_block, int size) {
        superBlock = get_super_block(disk_name);

        int position_in_entries_block;

        if(superBlock.entries_quantity < NUM_FILES)
            position_in_entries_block = (int) superBlock.entries_quantity;
        else
            position_in_entries_block = get_deleted_entry_position(disk_name);

        if(position_in_entries_block == -1){
            printf("You can't create more files, you've reached the maximun of %d",NUM_FILES);
            printf("\n");
            return -1;
        }

        int ya_existe = get_entry_by_name(disk_name,name);

        if(ya_existe != -1){
            printf("There's an existing file with this name \n");
            printf("\n");
            return -1;
        }

        entry entrada;

        //Crear los datos de la nueva entrada
        entrada.date_created = (uint32_t) (int) time(NULL);
        entrada.deleted = 0;
        strcpy((char *) &entrada.name, name);
        entrada.size = (unsigned int) size;
        entrada.first_block = first_block;

       // printf(entrada.date_created)

        //Escribir en archvivo
        char* buffer = (char*) malloc (BLOCK_SIZE);
        memcpy(buffer, &entrada, sizeof(entrada));

        util::write_entry_in_block(disk_name,buffer, 1, position_in_entries_block);

        delete[] buffer;

        //La actualizacion se llevará a cabo solo si el super bloque tiene menos de 128 archivos
        if(superBlock.entries_quantity < NUM_FILES){
            //Actualizar el superbloque
            superBlock.size = superBlock.size;
            superBlock.mounted = 1; //Si crea archivos es porque esta montado
            superBlock.block_count = superBlock.block_count;
            superBlock.bitmap_blocks = superBlock.bitmap_blocks;
            superBlock.entries_blocks = superBlock.entries_blocks;

            //Cambiar el numero de entradas de la tabla porque ya agregue el archivo
            superBlock.entries_quantity = superBlock.entries_quantity + 1;

            char* b = (char*) malloc (BLOCK_SIZE);
            memcpy(b, &superBlock, sizeof(superBlock));

            util::write_block(disk_name,b,0);

            delete[] b;
        }


        return 0;
    }

    super_block disk::get_super_block(char *name) {
        super_block leer;

        char* buffer2 = new char[BLOCK_SIZE];// malloc(sizeof(SuperBlock));
        util::read_block(name, buffer2, 0);
        memcpy(&leer, buffer2, sizeof(leer));

        delete[] buffer2;

        return leer;
    }

    void disk::make_ls(char * name) {
        superBlock = get_super_block(name);

        int no_deleted_entries = 0;

        if(superBlock.entries_quantity > 0){

            entry entradas [superBlock.entries_quantity] ;

            char* buffer2 = new char[BLOCK_SIZE];// malloc(sizeof(SuperBlock));
            util::read_block(name, buffer2, 1);
            memcpy(&entradas, buffer2, sizeof(entradas));

            delete[] buffer2;

            printf("\n");

            for (int i = 0; i < superBlock.entries_quantity; ++i) {
                if(!entradas[i].deleted){

                    no_deleted_entries+=1;

                    printf("Nombre: %s\n", (char *) entradas[i].name);
                    printf("Fecha creacion: ");

                    struct tm * timeinfo;
                    char bu[80];

                    timeinfo = localtime((const time_t *) &entradas[i].date_created);

                    strftime(bu,80,"%d-%m-%Y %I:%M:%S",timeinfo);
                    std::string str(bu);

                    std::cout << str ;

                    printf("\nTamanio archivo : %u bytes\n",entradas[i].size);
                    printf("Primer bloque data : %u\n", entradas[i].first_block);

                    printf("\n");
                }
            }
    }

    printf("\nCantidad de archivos en filesystem : %i\n",no_deleted_entries);
    printf("\n");

    }

    void disk::rename_file(char* disk_name,char *name, char *new_name) {
        int position_entry = get_entry_by_name(disk_name,name);

        int ya_existe = get_entry_by_name(disk_name,new_name);

        if(ya_existe != -1){
            printf("There's an existing file with this name \n");
            return;
        }

        if (position_entry != -1) {
            entry anterior;

            char *buffer2 = new char[sizeof(entry)];
            util::read_entry_in_block(disk_name,buffer2, 1, position_entry);
            memcpy(&anterior, buffer2, sizeof(entry));

            delete[] buffer2;

            printf("Nombre: %s\n", (char *) anterior.name);
            printf("Fecha creacion: ");

            struct tm *timeinfo;
            char bu[80];

            timeinfo = localtime((const time_t *) &anterior.date_created);

            strftime(bu, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
            std::string str(bu);

            std::cout << str;

            printf("\nTamanio archivo : %u\n", anterior.size);

            anterior.deleted = anterior.deleted;
            strcpy((char *) &anterior.name, new_name);
            anterior.date_created = anterior.date_created;
            anterior.size = anterior.size;
            anterior.first_block = anterior.first_block;

            char* buffer = (char*) malloc (BLOCK_SIZE);
            memcpy(buffer, &anterior, sizeof(anterior));

            util::write_entry_in_block(disk_name,buffer, 1, position_entry);

            delete[] buffer;
        } else{
            printf("\nThere's no file with that name \n");
            printf("\n");
        }
    }

    int disk::get_entry_by_name(char * disk_name,char *name) {
        superBlock = get_super_block(disk_name);

        if (superBlock.entries_quantity > 0) {
            entry entradas[superBlock.entries_quantity];

            char *buffer2 = new char[BLOCK_SIZE];// malloc(sizeof(SuperBlock));
            util::read_block(disk_name,buffer2, 1);
            memcpy(&entradas, buffer2, sizeof(entradas));

            delete[] buffer2;

            for (int i = 0; i < superBlock.entries_quantity; ++i) {
                if(!entradas[i].deleted){
                    if (strcmp((const char *) entradas[i].name, name) == 0){
                        //printf("numero %i\n", i);
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    void reset_bitmap(char* disk_name,int pos) {
        int bits_needed = (int) (superBlock.block_count / BIT);
        char  mapa_bits[bits_needed];

        //Tengo el mapa de bits en memoria
        char* buffer3 = new char[BLOCK_SIZE];
        util::read_bitmap_in_block(disk_name, buffer3, 2, (int) superBlock.bitmap_blocks);
        memcpy(&mapa_bits, buffer3, sizeof(mapa_bits));

       // delete[] buffer3;

        bitmapReset((byte *) mapa_bits, pos);

        //Escribir en disco los cambios hechos al mapa de bits
        // printf("Escribiendo mapa bits en disco \n");
        char* buffer4 = (char*) malloc (BLOCK_SIZE * superBlock.bitmap_blocks);
        memcpy(buffer4,  &mapa_bits, sizeof(&mapa_bits));

        util::write_bitmap_in_block(disk_name, buffer4, 2, (int) superBlock.bitmap_blocks);

       // delete[] buffer4;

    }

    void free_bitmap_from_deleted_file(char * disk_name,int pos){

        data_block dataBlock;

        char* buffer = new char[BLOCK_SIZE];
        util::read_block(disk_name,buffer,pos);
        memcpy(&dataBlock, buffer, sizeof(dataBlock));

      //  delete[] buffer;

        printf("pos %d\n", pos);
        printf("next block : %d\n", dataBlock.next_block);

        //Setear el bit como libre en el mapa de bits
        //  bitmapReset((byte *) &mapa_bits, pos);
        reset_bitmap(disk_name,pos);

        if (dataBlock.next_block != 0)
            return free_bitmap_from_deleted_file(disk_name,(unsigned int) dataBlock.next_block);

    }

    int disk::delete_file(char* disk_name,char *name) {
        int position_entry = get_entry_by_name(disk_name,name);
        printf("\n");

        if (position_entry != -1) {
            entry anterior;

            char *buffer2 = new char[sizeof(entry)];
            util::read_entry_in_block(disk_name,buffer2, 1, position_entry);
            memcpy(&anterior, buffer2, sizeof(entry));

            delete[] buffer2;

            printf("Nombre: %s\n", (char *) anterior.name);
            printf("Fecha creacion: ");

            struct tm *timeinfo;
            char bu[80];

            timeinfo = localtime((const time_t *) &anterior.date_created);

            strftime(bu, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
            std::string str(bu);

            std::cout << str;

            printf("\nTamanio archivo : %u\n", anterior.size);

            anterior.deleted = 1;
            strcpy((char *) &anterior.name, anterior.name);
            anterior.date_created = anterior.date_created;
            anterior.size = anterior.size;
            anterior.first_block = anterior.first_block;


            int tam_buffer = BLOCK_SIZE - sizeof(int);
            //Liberar los bloques ocupados en el mapa de bits
            //leyendo del disco mi mapa de bits
            int blocks_needed = (int) floor((anterior.size / (tam_buffer)) + 1);
            data_block dataBlock;
            int i=0;

            if(anterior.first_block != 0){
                //free_bitmap_from_deleted_file(disk_name,anterior.first_block);
                do{
                    if(i == 0){
                        char* buffer = new char[BLOCK_SIZE];
                        util::read_block(disk_name,buffer,anterior.first_block);
                        memcpy(&dataBlock, buffer, sizeof(dataBlock));

                        reset_bitmap(disk_name,anterior.first_block);
                        reset_bitmap(disk_name,dataBlock.next_block);

                        printf("anterior.first block %d\n", anterior.first_block);
                        printf("datablock.nextblock %d\n", dataBlock.next_block);

                        delete[] buffer;
                    } else{
                        if(i != blocks_needed-1){
                            char* buffer = new char[BLOCK_SIZE];
                            util::read_block(disk_name,buffer,dataBlock.next_block);
                            memcpy(&dataBlock, buffer, sizeof(dataBlock));

                            reset_bitmap(disk_name,dataBlock.next_block);
                            printf("datablock.nextblock %d\n", dataBlock.next_block);

                            delete[] buffer;
                        }
                        else{
                            char* buffer = new char[BLOCK_SIZE];
                            util::read_block(disk_name,buffer,dataBlock.next_block);
                            memcpy(&dataBlock, buffer, sizeof(dataBlock));

                            delete[] buffer;

                            reset_bitmap(disk_name,dataBlock.next_block);

                            printf("datablock.nextblock %d\n", dataBlock.next_block);
                        }
                    }

                    i++;

                }while (blocks_needed-1!=i);
            }

            char* buffer = (char*) malloc (BLOCK_SIZE);
            memcpy(buffer, &anterior, sizeof(anterior));

            util::write_entry_in_block(disk_name,buffer, 1, position_entry);

            delete[] buffer;
        } else{
            printf("\nThere's no file with that name \n");
            printf("\n");
            return -1;
        }

        return 0;
    }

    int disk::get_deleted_entry_position(char * disk_name) {
        superBlock = get_super_block(disk_name);

        if (superBlock.entries_quantity > 0) {
            entry entradas[superBlock.entries_quantity];

            char *buffer2 = new char[BLOCK_SIZE];
            util::read_block(disk_name,buffer2, 1);
            memcpy(&entradas, buffer2, sizeof(entradas));
            delete[] buffer2;

            for (int i = 0; i < superBlock.entries_quantity; ++i) {
                if (entradas[i].deleted == 1){
                    return i;
                }
            }
        }
        return -1;
    }




























