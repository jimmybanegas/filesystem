#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include "util.h"
#include "functions.h"

using namespace std;
//C:\Users\JIMMYR~1\Downloads\ipscan.exe

int main(int argc, char *argv[])
{
    try{
        int cmd;
        string disk_name;

        do{
            string cname2;
            string parametro1 ;
            string parametro2 ;
            string parametro3 ;

            printf("jimmybanegas93@linux:");
            printf("$ ");
            getline (cin, (string &) cname2);

            string buf; // Have a buffer string
            stringstream ss(cname2); // Insert the string into a stream

            vector<string> tokens; // Create vector to hold our words

            while (ss >> buf)
                tokens.push_back(buf);

            cname2 = (char *)tokens[0].c_str();

            if(tokens.size() == 2){
                parametro1 = (char *) tokens [1].c_str();
                parametro2 = ("");
                parametro3 = ("");
            }
            if(tokens.size() == 3){
                parametro1 = (char *) tokens [1].c_str();
                parametro2 = (char *) tokens [2].c_str();
                parametro3 = ("");
            }

            if(tokens.size() == 4){
                parametro1 = (char *) tokens [1].c_str();
                parametro2 = (char *) tokens [2].c_str();
                parametro3 = (char *) tokens [3].c_str();
            }

            if((strcmp("delete_block", (char*)cname2.c_str()) == 0 || strcmp("mount", (char*)cname2.c_str()) == 0
                || strcmp("empty", (char*)cname2.c_str()) == 0 || strcmp("delete", (char*)cname2.c_str()) == 0 )
               && tokens.size() != 2){

                printf("Check the parameters\r\n");
                util::findCmdMessage((char *)cname2.c_str());
                printf("\n");

                continue ;
            }

            if((strcmp("rename", (char*)cname2.c_str()) == 0
                || strcmp("copy_from_fs", (char*)cname2.c_str()) == 0 || strcmp("copy_to_fs", (char*)cname2.c_str()) == 0)
               && tokens.size() != 3){

                printf("Check the parameters \r\n");
                util::findCmdMessage((char *)cname2.c_str());
                printf("\n");

                continue ;
            }

            if((strcmp("create_block", (char*)cname2.c_str())  == 0)){
                if(strcmp("MB",(char*)parametro3.c_str()) != 0 && strcmp("GB",(char*)parametro3.c_str()) != 0 || tokens.size() != 4){
                    printf("Check the parameters \r\n");
                    util::findCmdMessage((char *)cname2.c_str());
                    printf("\n");

                    continue ;
                }
            }

            if(strcmp("mount", (char*)cname2.c_str()) == 0)
                disk_name = parametro1;

            cmd = util::findCmd((char *) cname2.c_str()); // map cname to an index

            switch(cmd)
            {
                case 0 : functions::menu();	break;
                case 1 : functions::ls((char *) disk_name.c_str()); break;
                case 2 : functions::make_disk((char *) parametro1.c_str(),(unsigned int) atoi(parametro2.c_str() ),(char *) parametro3.c_str() ); break;
                case 3 : functions::create_file((char *) disk_name.c_str(), (char *) parametro1.c_str()); break;
                case 4 : functions::unlink((char *) disk_name.c_str(), (char *) parametro1.c_str()); break;
                case 5 : functions::rename((char *) disk_name.c_str(),(char *) parametro1.c_str(), (char *) parametro2.c_str());  break;
                case 6 : functions::copy_in((char *) disk_name.c_str(), (char *) parametro1.c_str(),(char *) parametro2.c_str()); break;
                case 7 : functions::copy_out((char *) disk_name.c_str(), (char *) parametro1.c_str(),(char *) parametro2.c_str()); break;
                case 8 : functions::mount((char *) parametro1.c_str()); break;
                case 9 : functions::unmount((char *) disk_name.c_str()); break;
                case 10: functions::info((char *) disk_name.c_str()); break;
                case 11: functions::quit((char *) disk_name.c_str()); break;
                case 12: functions::delete_disk((char *) parametro1.c_str()); break;
                case 13 : system("clear");	break;
                default: cout<<"invalid command\n"<<endl;
                    break;

            }

        }while(1);
    }
    catch (exception e){

    }
}