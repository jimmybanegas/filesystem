//
// Created by Affisa-Jimmy on 23/8/2016.
//

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include "functions.h"
#include "disk.h"
#include "util.h"
#include "bitmap.h"
#include "data_block.h"

using namespace std;

    void functions::menu() {
        printf("ls: list directories\n");
        printf("create_block <device_name> <size>: Creates new partition\n");
        printf("delete_block <device_name> : Creates new partition\n");
        printf("empty <filename>: Creates a new empty file\n");
        printf("delete <filename>: Deletes a file\n");
        printf("rename <old_name> <new_name>: Renames a file\n");
        printf("copy_from_fs <source_path_fs> <target_filename_myfs>: Copies a file to our filesystem\n");
        printf("copy_to_fs <source_filename_myfs> <target_path_fs> : Copies a file to another filesystem\n");
        printf("mount <device_name>: Mounts a created partition\n");
        printf("unmount: Unmounts current partition\n");
        printf("info: Shows current partition metadata\n");
        printf("quit: To Exit FS\n");
        printf("\n");
    }

    void functions::quit(char * disk_name) {
        if(strcmp(disk_name,"") != 0){
            super_block super = disk::get_super_block(disk_name);

            if(super.mounted){
                //disk::close_disk(globalDisk);
                unmount(disk_name);
            }
        }

        exit(0);
    }

    void functions::mount(char *disk_name) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);

        if(super.mounted){
            printf("There is a mounted disk, please unmount it before \n");
            printf("\n");
            return;
        }

        super_block leer;

        char* buffer2 = new char[BLOCK_SIZE];// malloc(sizeof(SuperBlock));

        int existe = util::read_block(disk_name,buffer2,0);
        memcpy(&leer, buffer2, sizeof(leer));
        delete[] buffer2;

        if(existe == -1){
            printf("Could not mount disk\n");
            printf("\n");
        } else{
            leer.mounted = 1;
            //Reescribir superbloque para indicar que está montado ahora
            char* buffer = (char*) malloc (BLOCK_SIZE);
            memcpy(buffer, &leer, sizeof(leer));

            util::write_block(disk_name,buffer, 0);
            delete[] buffer;

            printf("Disk mounted\n");
            printf("\n");
        }
    }

    void functions::ls(char *disk_name) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);
        if(super.mounted)
            disk::make_ls(disk_name);
        else
            printf("No mounted disk, please mount one \n");
            printf("\n");
    }

    void functions::make_disk(char *name,uint64_t disk_size, char *uni) {
        //Calcular la cantidad de bloque que tendrá el archivo, asumiendo que el disk_size viene en MB
        uint64_t disk_blocks;

        if(strcmp("MB",uni) == 0)
            disk_blocks = ((disk_size * 1024 * 1024) / BLOCK_SIZE);
        else  if(strcmp("GB",uni) == 0)
            disk_blocks = ((disk_size * 1024 * 1024 * 1024) / BLOCK_SIZE);

        disk::make_disk(name, &disk_blocks, &disk_size);
    }

    void functions::create_file(char *disk_name, char *file_name) {

        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);
        if(super.mounted){
            int x = disk::make_file(disk_name, file_name,0,0);

            if(x == 0){
                printf("File created succesfully \n");
                printf("\n");
            }
            else
                printf("File not created \n");
                printf("\n");

        } else{
            printf("No mounted disk, please mount one \n");
            printf("\n");
        }
    }

    void functions::unlink(char *disk_name, char *file_name) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);
        if(super.mounted)
            int x = disk::delete_file(disk_name,file_name);
        else
            printf("No mounted disk, please mount one \n");
        printf("\n");
    }

    void functions::rename(char *disk_name,char *old_name, char *new_name) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);
        if(super.mounted)
            disk::rename_file(disk_name,old_name,new_name);
        else
            printf("No mounted disk, please mount one \n");
            printf("\n");

    }

    void functions::copy_in(char *disk_name, char *origin_path,char *filename) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block superBlock = disk::get_super_block(disk_name);

        if(!superBlock.mounted){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        //Esta variable controla el tamano de datos de cada bloque de datos, recordar que al final hay un apuntador al proximo
        int tam_buffer = BLOCK_SIZE - sizeof(int);

        //C:\Users\Affisa-Jimmy\Downloads\rufus-2.10.exe
        //C:\Users\Public\Downloads\vs14-kb3165756.exe
        //C:\Users\Public\Downloads\SSDTSetup.exe
        //C:\Users\Public\Downloads\HSFK.exe
        //C:\Users\Public\Downloads\EjemploProyecto.7z
        //C:\Users\Public\Downloads\ResetApp.exe
        //C:\Users\JIMMYR~1\Downloads\ipscan.exe
        //C:\Users\JIMMYR~1\Documents\
        //cmd /c for %A in ("C:\Documents and Settings\User\NTUSER.DAT") do @echo %~sA

        //Tamano del archivo que recibo en bytes
        streampos size_in_bytes;
        char * memblock;

        ifstream file (origin_path, ios::in|ios::binary|ios::ate);
        if (file.is_open())
        {
            size_in_bytes = file.tellg();
            memblock = new char [size_in_bytes];
            file.seekg (0, ios::beg);
            file.read (memblock, size_in_bytes);
            file.close();

            delete[] memblock;
        }
        else {
            printf("Unable to open file\n");
            printf("\n");
            return;
        }

        //Cantidad de bloques necesarios para escribir esos bytes
        //Cantidad de bloques que va a mecesitar el archivo para ser escrito
        int blocks_needed = (int) floor((size_in_bytes / (tam_buffer)) + 1);
        //Se le suma uno por aquellos que ocupan menos de un bloque, se les dara al menos 1

        if(blocks_needed > superBlock.block_count){
            printf("There's no enough space on disk\n");
            printf("\n");
            return;
        }

        //Size of data = superblock size menos dos bloques de metadata, menos bloques de bitmap
        if(size_in_bytes > superBlock.size - (2*BLOCK_SIZE) - (superBlock.bitmap_blocks *BLOCK_SIZE)){
            printf("There's no enough space on disk\n");
            printf("\n");
        }

        //Encontrar el proximo bit libre
        //leyendo del disco mi mapa de bits
        int bits_needed = (int) (superBlock.block_count / BIT);
        char  mapa_bits[bits_needed];

        //Tengo el mapa de bits en memoria
        char* buffer2 = new char[BLOCK_SIZE*superBlock.bitmap_blocks];// malloc(sizeof(SuperBlock));
        util::read_bitmap_in_block(disk_name,buffer2, 2, (int) superBlock.bitmap_blocks);
        memcpy(&mapa_bits, buffer2, sizeof(mapa_bits));

        delete[] buffer2;

        //Consultar si hay bloque libre, sino, decir que no hay espacio
        int free_block = bitmapSearch((byte *) mapa_bits,0,bits_needed,0);

     //   printf("Primer libre: %d\n",free_block);
        //printf("Primer libre: %d\n",free_block);

        if(free_block == -1){
            printf("There's no free space on disk\n");
            printf("\n");
            return;
        }

        int first_data_block = free_block;

        //Setear como ocupado el primer bloque de este archivo
        bitmapSet((byte *) mapa_bits, free_block);

        int success = 1;
        std::ifstream infile (origin_path ,ios::in|ios::binary);

        //Hacer buffers de tamaño 4088  bytes
        for (int i = 0; i < blocks_needed; i++)
        {
            data_block dataBlock;

            if(i != 0){
                free_block = bitmapSearch((byte *) mapa_bits,0,bits_needed,0);
                bitmapSet((byte *) mapa_bits, free_block);
            }

            int next_block = bitmapSearch((byte *) mapa_bits,0,bits_needed,0);

         //   printf("next block :%d\n",next_block);

            if(next_block == -1){
                printf("There's no free space on disk\n");
                printf("\n");
                success = -1;
                break;
            }

            //Posicionarme en el archivo primero vez sera en o, segunda en 4088, tercera en 12264
            infile.seekg((i * tam_buffer) , ios::beg);

            //Leer 4088 bytes y asignarlos al bufer de el datablock
            if(i != blocks_needed-1){
                infile.read (dataBlock.data, tam_buffer);
            } else{
                infile.read (dataBlock.data, abs((i*tam_buffer)-size_in_bytes));
                printf("ultimo :%d\n",abs((i*tam_buffer)-size_in_bytes));
                printf("ultimo :%d\n",dataBlock.next_block);
            }

          //  memcpy((void *) dataBlock.data, data, (size_t) tam_buffer);
          //  infile.close();

            //Si ya es el ultimo bloque, hacer que apunte a nulo porque ya termina la lista enlazada
            if(i == blocks_needed-1){
                dataBlock.next_block = 0;
            } else{
                // free_block = bitmapSearch((byte *) mapa_bits,0,bits_needed,0);
                dataBlock.next_block = next_block;
            }

            // Alocar el tamano de un bloque para escribir el datablock
            char* buffer = (char*) malloc (BLOCK_SIZE);
            memcpy(buffer, &dataBlock, BLOCK_SIZE);

            //printf("libre leido diso :%d\n",free_block);
            util::write_block(disk_name,buffer,free_block);

            delete[] buffer;
        }

        infile.close();

        if(success!= -1){

            //Si tod sale bien, escribir la en la entrada en disco
            //Crear la entrada en la tabla de directorios
            int x = disk::make_file(disk_name,filename, (unsigned int) first_data_block, (int) size_in_bytes);

            if(x == 0){
                printf("File created succesfully \n");
                printf("\n");
            }
            else
                printf("File not created \n");
                printf("\n");


            //Reescribir el mapa de bits con todos los cambios y los bloques ocupados
            char* buffer3 = (char*) malloc (BLOCK_SIZE * superBlock.bitmap_blocks);
            memcpy(buffer3, &mapa_bits, sizeof(mapa_bits));

            util::write_bitmap_in_block(disk_name,buffer3, 2, (int) superBlock.bitmap_blocks);
            delete[] buffer3;
        }
    }

    char *name;
    void get_data_from_block(char *disk_name,ofstream &output_file, unsigned int pos, int size_in_bytes, int i) {
        int tam_buffer = BLOCK_SIZE - sizeof(int);
        int blocks_needed = (int) floor((size_in_bytes / (tam_buffer)) + 1);
        //char *name = disk_name;

        if(i==0)
            name =  disk_name;

        data_block dataBlock;

        if(i != blocks_needed-1){
            char* buffer = new char[BLOCK_SIZE];
            util::read_block(name,buffer,pos);
            memcpy(&dataBlock, buffer, sizeof(dataBlock));
           // delete[] buffer;

            output_file.write(dataBlock.data, tam_buffer);
        }
        else{
            char* buffer = new char[BLOCK_SIZE];
            util::read_block(name,buffer,pos);
            memcpy(&dataBlock, buffer, sizeof(dataBlock));

            delete[] buffer;

            printf("entro %i \n", abs((i*tam_buffer)-size_in_bytes));

            output_file.write(dataBlock.data, abs((i*tam_buffer)-size_in_bytes));
        }

        i++;

        printf("pos %d\n", pos);
        printf("leido next block : %d\n", dataBlock.next_block);

        if (dataBlock.next_block != 0)
            return get_data_from_block(name, output_file, (unsigned int) dataBlock.next_block,size_in_bytes,i );

        output_file.close();

    }

    void functions::copy_out(char *disk_name, char *filename, char *destiny_path) {

        try
        {
            if(strcmp(disk_name,"") == 0){
                printf("No mounted disk, please mount one \n");
                printf("\n");
                return;
            }

            super_block superBlock = disk::get_super_block(disk_name);

            if(!superBlock.mounted){
                printf("No mounted disk, please mount one \n");
                printf("\n");
                return;
            }

            int tam_buffer = BLOCK_SIZE - sizeof(int);
            //Obtener la posicion del archivo que quiero exportar
            int position_entry = disk::get_entry_by_name(disk_name,filename);

            if (position_entry != -1) {
                entry anterior;

                char *buffer2 = new char[sizeof(entry)];
                util::read_entry_in_block(disk_name,buffer2, 1, position_entry);
                memcpy(&anterior, buffer2, sizeof(entry));

                delete[] buffer2;

                printf("Nombre: %s\n", (char *) anterior.name);
                printf("Fecha creacion: ");

                struct tm *timeinfo;
                char bu[80];

                timeinfo = localtime((const time_t *) &anterior.date_created);

                strftime(bu, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
                std::string str(bu);

                std::cout << str;

                printf("\nTamanio archivo : %u\n", anterior.size);
                printf("\n");

                std::ofstream output_file(destiny_path,  std::ios::binary);

                int blocks_needed = (int) floor((anterior.size / (tam_buffer)) + 1);

                data_block dataBlock;
                int i=0;

                do{
                    if(i == 0){
                        char* buffer = new char[BLOCK_SIZE];
                        util::read_block(disk_name,buffer,anterior.first_block);
                        memcpy(&dataBlock, buffer, sizeof(dataBlock));

                        output_file.write(dataBlock.data, tam_buffer);

                        delete[] buffer;
                    } else{
                        if(i != blocks_needed-1){
                            char* buffer = new char[BLOCK_SIZE];
                            util::read_block(disk_name,buffer,dataBlock.next_block);
                            memcpy(&dataBlock, buffer, sizeof(dataBlock));

                            output_file.write(dataBlock.data, tam_buffer);

                         //   delete[] buffer;
                        }
                        else{
                            char* buffer = new char[BLOCK_SIZE];
                            util::read_block(disk_name,buffer,dataBlock.next_block);
                            memcpy(&dataBlock, buffer, sizeof(dataBlock));

                            delete[] buffer;

                            output_file.write(dataBlock.data, abs((i*tam_buffer)-(int)anterior.size));
                        }
                    }

                    i++;

                }while (blocks_needed!=i);

                //  get_data_from_block(disk_name, output_file, anterior.first_block,anterior.size,0);

                output_file.close();
            }
        }
        catch (int e)
        {
            //cout << "An exception occurred. Exception Nr. " << e << '\n';
        }
    }

    void functions::unmount(char *name) {
        super_block super = disk::get_super_block(name);
        if(super.mounted)
            disk::close_disk(name);
        else
            printf("No mounted disk, please mount one \n");
            printf("\n");

    }

    void functions::info(char *disk_name) {
        if(strcmp(disk_name,"") == 0){
            printf("No mounted disk, please mount one \n");
            printf("\n");
            return;
        }

        super_block super = disk::get_super_block(disk_name);
        if(super.mounted)
            disk::show_information(disk_name);
        else
            printf("No mounted disk, please mount one \n");
            printf("\n");

    }

    void functions::delete_disk(char *disk_name) {
        int ret;
        FILE *fp;

        fp = fopen(disk_name, "w");

        fclose(fp);

        ret = remove(disk_name);

        if(ret == 0)
        {
            printf("File deleted successfully\n");
            printf("\n");
        }
        else
        {
            printf("Error: unable to delete the file\n");
            printf("\n");
        }

    }





























