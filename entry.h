//
// Created by Affisa-Jimmy on 23/8/2016.
//

#ifndef FILESYSTEM_ENTRADADETABLA_H
#define FILESYSTEM_ENTRADADETABLA_H

#include <cstdint>

#define NAMELEN 16          /* the max length of a file name */

class entry {

public:
    uint32_t deleted; /*indicador si el archivo está borrado o no */
    char name[NAMELEN]; /* name of the file, not null-terminated */
    uint32_t  date_created; /* date created */
    unsigned int size; /* size of the file*/
    unsigned int first_block; /*pointer to the first block of data*/
};


#endif //FILESYSTEM_ENTRADADETABLA_H
