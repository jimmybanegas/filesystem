//
// Created by Affisa-Jimmy on 23/8/2016.
//

#ifndef FILESYSTEM_FUNCIONES_H
#define FILESYSTEM_FUNCIONES_H


class functions {
public:
    static void menu();

    static void quit(char *disk_name);

    static void mount(char *disk_name);

    static void ls(char *disk_name);

    static void create_file(char *disk_name, char *file_name);

    static void unlink(char *disk_name, char *filename);

    static void make_disk(char* disk_name,uint64_t disk_size, char *uni); //tameño en del sico

    static void rename(char *disk_name, char *old_name, char *new_name);

    static void copy_in(char *disk_name, char *origin_path, char *filename);

    static void copy_out(char *disk_name,char *filename, char *destiny_path);

    static void info(char *disk_name);

    static void unmount(char * disk_name);

    static void delete_disk(char *disk_name);
};


#endif //FILESYSTEM_FUNCIONES_H
