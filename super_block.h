//
// Created by Jimmy Ramos on 25-Aug-16.
//

#ifndef FILESYSTEM_SUPERBLOQUE_H
#define FILESYSTEM_SUPERBLOQUE_H


#include <stdlib.h>
#include <stdint.h>

class super_block {
public:
    uint64_t size; //Tama;o del disco
	//char* name[20]; /* name of the file, not null-terminated */
    uint64_t mounted; // flag para ver si el disco esta montado o no
    uint64_t block_count; // Cantidad de bloques de 4 kb  que tiene el disco
    uint64_t bitmap_blocks; //Cantidad de bloques ocupados por el mapa de bits
    uint64_t entries_blocks ; //Cantidad de bloques ocupados por el la tabla de entradas
    uint64_t entries_quantity; //Cantidad de archivos en mi tabla de entradas

};


#endif //FILESYSTEM_SUPERBLOQUE_H
