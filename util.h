//
// Created by Jimmy Ramos on 21-Aug-16.
//

#ifndef FILESYSTEM_UTIL_H
#define FILESYSTEM_UTIL_H


#include <cstdio>

class util {
    public:
        static int findCmd(char* command);

        static void findCmdMessage(char* command);

        static int read_block(char *disk,char *buf, int block_number);

        static int write_block(char * disk, char *buf, int block_number);

        static int write_entry_in_block(char *disk_name, char *buf, int block_number, int entries_quantity);

        static int read_bitmap_in_block(char *disk_name, char *buf, int block_number, int blocks_quantity);

        static int write_bitmap_in_block(char *disk_name, char *buf, int block_number, int blocks_quantity);

        static int read_entry_in_block(char *disk_name, char *buf, int block_number, int entry_position);

        int write_block_copyin(char *disk_name, char *buf, int block_number, int prox_bloque);
};


#endif //FILESYSTEM_UTIL_H
